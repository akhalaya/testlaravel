<?php
/**
 * ${DESCRIPTION}
 *
 * @package laravel
 */

namespace Bfm\Test;

use Illuminate\Support\ServiceProvider;

class ExampleServiceProvider extends ServiceProvider {

    public function boot(){

        require_once __DIR__.'/Http/router.php';

        $this->loadViewsFrom(__DIR__.'/Views', 'example');

    }

    public function register()

    {

        $this->app['example'] = $this->app->share(function(){

            return new Example;

        });

    }

}