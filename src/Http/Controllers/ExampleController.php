<?php
/**
 * ${DESCRIPTION}
 *
 * @package laravel
 */

namespace Bfm\Test\Http\Controllers;

use Illuminate\Routing\Controller;

class ExampleController extends Controller {

    public function index() {

        return view('example::index');

    }

}