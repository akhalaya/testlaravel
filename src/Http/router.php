<?php
/**
 * ${DESCRIPTION}
 *
 * @package laravel
 */

Route::group(['namespace'=>'Bfm\Test\Http\Controllers', 'prefix' => 'example'], function() {

    Route::get('/','ExampleController@index');

});